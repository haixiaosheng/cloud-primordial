#### 一、docker构建命令
```shell
root@haixiaosheng:~# docker build -t haixiaoshengcloud/http-v0.2 .
Sending build context to Docker daemon  6.144kB
Step 1/9 : FROM golang:1.17.2-alpine3.14 as builder
 ---> 35cd8c8897b1
Step 2/9 : WORKDIR /apps
 ---> Using cache
 ---> 7ebd15442f0d
Step 3/9 : COPY . .
 ---> cd6463aa52c2
Step 4/9 : RUN CGO_ENALBED=0 GOOS=linux go build -o httpserver
 ---> Running in 3122a4275b88
Removing intermediate container 3122a4275b88
 ---> c0ff2c57d78d
Step 5/9 : FROM alpine:latest as prod
 ---> 14119a10abf4
Step 6/9 : WORKDIR /apps
 ---> Running in a77b233865c5
Removing intermediate container a77b233865c5
 ---> 2c1ce73d2faf
Step 7/9 : COPY --from=builder /apps/httpserver /apps
 ---> d33b94e45047
Step 8/9 : EXPOSE 8080
 ---> Running in 7207ddd7e570
Removing intermediate container 7207ddd7e570
 ---> f95979319659
Step 9/9 : CMD ["./httpserver"]
 ---> Running in a4c860c2eaa9
Removing intermediate container a4c860c2eaa9
 ---> f32b6340a3cb
Successfully built f32b6340a3cb
Successfully tagged haixiaoshengcloud/http-v0.2:latest

root@haixiaosheng:~# docker images
REPOSITORY                    TAG                 IMAGE ID       CREATED          SIZE
haixiaoshengcloud/http-v0.2   latest              f32b6340a3cb   24 minutes ago   11.7MB
```
最佳实践：
    1、最大限度减少镜像层数量；
    2、分阶段build，最终只需要在一个很小的image里。

#### 二、启动http docker命令
```shell
# docker run -d --name http-v0.2 -p 80:8080 haixiaoshengcloud/http-v0.2:latest
CONTAINER ID   IMAGE                                COMMAND          CREATED         STATUS         PORTS                                   NAMES
98eb45af9e88   haixiaoshengcloud/http-v0.2:latest   "./httpserver"   5 seconds ago   Up 4 seconds   0.0.0.0:80->8080/tcp, :::80->8080/tcp   http-v0.2
```

#### 三、查看运行容器IP信息
```shell
root@haixiaosheng:~# nsenter --target $(docker inspect -f {{.State.Pid}} 98eb45af9e88) -n ip a            
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
10: eth0@if11: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
root@haixiaosheng:~# 
```

#### 四、推送docker images 至Docker Hub
```shell
docker push haixiaoshengcloud/http-v0.2:latest
```
