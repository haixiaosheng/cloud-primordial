package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	client := &http.Client{}
	reqUrl := "http://localhost:8080/"

	req, err := http.NewRequest("GET", reqUrl, nil)
	if err != nil {
		log.Fatal(err)
		return
	}

	req.Header.Add("Class-No", "2")
	fmt.Println(req.Header)
	ret, _  := client.Do(req)

    defer func() {
    	fmt.Println(ret.StatusCode)
    	_ = ret.Body.Close()
	}()
}
