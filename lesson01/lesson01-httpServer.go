package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func respHandler(w http.ResponseWriter, r *http.Request) {  // /处理函数
	v := r.Header.Get("Class-No")  // 获取请求头中Class-No键值信息
	// fmt.Println(v)
	w.Header().Add("Class-No", v)  // 写入响应头中

	val := getOSEnvVar("VERSION")  // 获取系统变量VERSION
	w.Header().Add("VERSION", val)  // 写入响应头中

	rAddr := r.RemoteAddr  // 获取客户端地址
	// http.Response{}
	rCode := 200
	w.WriteHeader(rCode)  // 写入状态码

	recordReqInfo(rAddr, rCode)  // 记录客户端地址以及状态码
}

func hzHandler(w http.ResponseWriter, r *http.Request) {  // /healthz处理函数
	w.WriteHeader(200)  // Response Header写入StatusCode
	var wBody []byte
	wBody = []byte("200")
	_, err := w.Write(wBody)  // 200状态码写入Respone Body中
	if err != nil {
		log.Fatal(err)
		return
	}

	recordReqInfo(r.RemoteAddr, 200)
}

func recordReqInfo(addr string, code int) {  // 记录客户端地址以及响应码函数
	fmt.Printf("客户端地址：%s, 响应码：%d\n", addr, code)
}

func getOSEnvVar(key string) (value string) {  // 获取系统变量函数
	return os.Getenv(key)
}

func main() {

	http.HandleFunc("/", respHandler)  // 访问/路径路由
	http.HandleFunc("/healthz", hzHandler)  // 访问/healthz路径路由
	_ = http.ListenAndServe(":8080", nil)  //监听地址以及监听处理函数

}
